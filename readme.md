This project is predicting the next day DOGE-USD trend. 

If you are using M1 chip Mac, I suggest the you can copy the code and run on google colab.

If you would like to use it on you own pc, please install the follow packages before run the code. 
1. scikit-learn  \
https://scikit-learn.org/stable/install.html

2. numpy  \
https://numpy.org/install/

3. pandas  \
https://pandas.pydata.org/getting_started.html

4. yfinance  \
https://pypi.org/project/yfinance/

5. Ta-Lib  \
https://mrjbq7.github.io/ta-lib/install.html

6. graphviz  \
https://pypi.org/project/graphviz/

7. matplotlib  \
https://pypi.org/project/matplotlib/